<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/main.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../JS/clock.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="Zacetna_stran.jsp">problem_gone.com</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="tezave.jsp">Tezave
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="vse_tezave.jsp">Vse tezave</a></li>
                </ul>
            </li>
            <li><a href="registered.jsp">Stevilo registriranih ponudnikov</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="registracija.jsp"><span class="glyphicon glyphicon-user"></span> Registracija</a></li>
            <li><a href="login.jsp"><span class="glyphicon glyphicon-log-in"></span> Prijava</a></li>
        </ul>
    </div>
</nav>
<form action="overjanje.jsp" method="post">
    <input type="hidden" name="action" value="login">
    <div class="container">
        <h1>Prijava</h1>
        <p>Prosimo, izpolnite obrazec za prijavo.</p>

        <hr>
        <label for="upo"><b>Uporabnisko Ime</b></label>
        <input type="text" placeholder="Vpisite Uporabnisko Ime" name="upo-ime" required>
<br>
<br>
        <label for="geslo"><b>Geslo</b></label>
        <input type="password" placeholder="Vpisite Geslo" name="geslo" required>



        <button type="submit" name="submit" class="prijavabtn">Prijava</button>
    </div>

    <div class="container signin">
        <p>Se nimate racuna? <a href="registracija.jsp">Registracija</a>.</p>
    </div>
</form>
</body>
</html>