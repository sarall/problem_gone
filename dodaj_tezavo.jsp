<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Astraea</title>
<link href="css/styles.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="/resources/demos/style.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
	  $( function() {
	    $( "#datepicker" ).datepicker();
	  } );
	</script>
	
</head>
<body>
<br></br>
<div class="" align="center">
   <img class="" src="slike/logo.jpg"></img>
</div>
<div class="menu-wrapper">
  <div class="menu">
    <ul>
      <li><a href="index.html" class="active">Domov</a></li>
    </ul>
  </div>
</div>
<!--- menu-wrapper div end -->
<div class="clearing"></div>
<div class="border-bottom"></div>
<div class="clearing"></div>
<div class="banner-wrapper">
  <div class="bannerlef"><img src="slike/banner-wrap-left.jpg" /></div>
  <div class="banner-container">
    <div class="container">
		<br></br>
			<h1 align="center">Dodaj tezavo</h1>
	  		<br></br>
	  		
	  		<div id="podatkiOseba" align="center">
	  		<form method="post" action="add_problem.jsp">
	  		<table align="center" cellspacing="10">
	  			<tr><th>Podatki</th></tr>
	  			<tr>
					<td>Ime tezave</td>
					<td><input type="text" name="naziv_t"></input></td>
				</tr>
				<tr>
					<td>Kategorija tezave</td>
					<td><input type="text" name="kategorija"></input></td>
				</tr>
				<tr>
					<td>Opis </td>
					<td><input type="text" name="opis"></input></td>
				</tr>
				<tr>
					<td>Datum</td>
					<td><input id="datepicker" name="datum" id="datum" type="text"></input></td>
				</tr>
	  		</table>
	  		<br></br>
	  			<input type="submit" value="Zakljuci vnos tezave!" />
	  		</form>
	  		</div>
	  </div>
  </div>
  <div class="bannerright"><img src="slike/banner-wrap-right.jpg"/></div>
<!--- banner wrapper div end -->
</div>
<div class="footer-wrapper">
  <div class="footer">
    <p> &copy; Astraea d.o.o. 2017 </p>
  </div>
</div>
</body>
</html>