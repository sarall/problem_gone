<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="povezava.*"%>
<%@ page import ="java.text.*" %>
<%@ page import ="java.sql.*" %>
<%@page import="java.sql.Date"%>
<%
	
    Connection connection = null;
    Statement statement = null;
    ResultSet rs = null;
    
    connection = Povezava_PB.povezi();
    statement = connection.createStatement();
    
    
    
		String naziv_t = request.getParameter("naziv_t");
		String kategorija_t = request.getParameter("kategorija");
		String opis_t = request.getParameter("opis");
		
		String datum = request.getParameter("datum");
		DateFormat df=new SimpleDateFormat("MM/dd/yyyy");

		java.util.Date dtt = df.parse(datum);
		java.sql.Date ds = new java.sql.Date(dtt.getTime());
		
		int nova_tezava = 0;
		int kontakt_o = 0;
		
		String tezava = "insert into Tezava (naziv_t, kategorija, opis, datum) values (?,?,?,?)";
		PreparedStatement a = connection.prepareStatement(tezava, Statement.RETURN_GENERATED_KEYS);
			a.setString(1, naziv_t);
			a.setString(2, kategorija_t);
			a.setString(3, opis_t);
			a.setDate(4, ds);
			
			int i_tezava = a.executeUpdate();
			
			if (i_tezava > 0) {
				rs = a.getGeneratedKeys();
				response.sendRedirect("zacetna.jsp");
				return;
				
			}
		
	
			else {
				out.println("Vnos tezave ni uspel! <a href='dodaj_tezavo.jsp'> Poskusi znova </a>");
			}
    
%>